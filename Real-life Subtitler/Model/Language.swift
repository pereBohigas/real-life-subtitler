//
//  Language.swift
//  Real-life Subtitler
//
//  Created by Peter on 30.07.19.
//

import Foundation

struct Language: Equatable {
    var code: String
    var name: String
    var regions: [(code: String, name: String)] = .init()
    
    init (_ languageCode: String, _ regionCode: String) {
        self.code = languageCode
        self.name = Locale.current.localizedString(forLanguageCode: languageCode)?.capitalizingFirstLetter() ?? ""
        addRegion(regionCode: regionCode)
    }
    
    private func getRegionNameFromCode(_ regionCode: String) -> String {
        return Locale.current.localizedString(forRegionCode: regionCode) ?? ""
    }
    
    mutating func addRegion(regionCode: String) {
        self.regions.append((code: regionCode, name: getRegionNameFromCode(regionCode)))
        sortRegions()
    }
    
    private mutating func sortRegions() {
        self.regions = self.regions.sorted(by: {$0.name < $1.name})
    }
    
    static func == (lhs: Language, rhs: Language) -> Bool {
        return lhs.code == rhs.code
    }
    
//    static func < (lhs: Language, rhs: Language) -> Bool{
//        return lhs.name < rhs.name
//    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
